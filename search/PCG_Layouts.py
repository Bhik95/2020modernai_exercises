## This is a file to create pacman maps via a constructive approach
import numpy as np
import sys
#define variables of the layout
levelMap = {'capsules': 'o',
            'Ghosts': 'G',
            'Pacman': 'P',
            'wall': '%',
            'food': '.'}

#define main variables
gridSize =[10,10]
maxCapsules = 2
maxGhosts = 3

def CreateLevel(ghosts, capsules,gridSize, levelMap):
    '''
    input variables
    ghosts = int with the information of how many ghosts exist in the level
    capsules = int with the information of how many capsules are in the level
    gridSize = the gridsize of the game
    levelMap = the dictionary with the information about the different objects on the level.
    '''
    goPositions = [] # GameObject Positions
    level = []
    for row in range(gridSize[0]):
        if row <= 0 or row >= gridSize[0]-1:
            level += [levelMap['wall']]*gridSize[1]+['\n']
            continue
        for col in range(gridSize[1]):
            if col <= 0 or col >= gridSize[1]-1:
                level += [levelMap['wall']]
            else:
                level += [levelMap['food']]
        level += ['\n']
    vertical = list(range(1, gridSize[0]-1))
    horizontal = list(range(1, gridSize[1]-1))
    pacPos = GetPositions(1, vertical, horizontal, goPositions, level, ['Pacman'],
                         gridSize=gridSize, levelMap=levelMap)
    goPositions += pacPos
    # your code HERE
    # Use the same thing that you have used for pacman with the ghosts and capsules
    return level

def GetPositions(amount, vpos, hpos, goPos, level, keyVals, gridSize, levelMap):
    '''
    input variables
    amount = the int representing the amount of values you want from the given game object
    vpos , hpos = the list of positions where the game object should or can be put on
    goPos = list of the gameobjects that are already in the game
    level = list with the layout of the game.
    keyvals = list of all the values of the given game object that could be set in the layout
            Note that this just works if you have different type of ghosts or things like that
    gridSize = the gridsize of the game, used to print the values on the level
    levelMap = the dictionary with the information about the different objects on the level.
    '''
    vertPos = np.random.choice(vpos, size=amount) # you could use randint instead of this.
    horPos = np.random.choice(hpos, size=amount)
    positions = [(x, y) for x, y in zip(vertPos, horPos)]
    while True:
        counter = 0
        for i in range(len(positions)):
            pos = positions[i]
            tmpPositions = positions.copy()
            tmpPositions.remove(pos) # The variable is removed to make sure we do not go to a infinite loop.
            # your code HERE
            # check ig the position that you chose for the item is already on the game object positions
            # or you have already selected
            if (pos in []):
                # if the position is in the lists, then create sample a new position
                # and swap it for the tuple in position i
                continue
            elif len(goPos) != 0:
                # this is an example of how specific rules that you can add to your pacman algorithm
                if goPos[0][0] - 1 <= pos[0] <= goPos[0][0] + 1 and goPos[0][1] - 1 <= pos[1] <= goPos[0][1] + 1:
                    print(pos, goPos[0])
                    # This assumes that the avatar position is in gpos[0] and
                    # is to make the avatar do not have items in a surrounding square.
                    vtmp = np.random.choice(vpos, size=1)[0]
                    htmp = np.random.choice(hpos, size=1)[0]
                    positions[i] = (vtmp, htmp)
                    continue
            counter += 1
        if counter == len(positions):
            break
    for pos in positions:
        i = pos[0] * (gridSize[0]+1) + pos[1]
        val = np.random.choice(keyVals, size=1)[0]
        # Add the value from the level map to the level at position i
    return positions

def WriteLevel(level, path="layouts/", game="pacman", lvl = 0):
    file = path + game + "_lvl" + str(lvl) + ".lay"
    f = open(file, "w+")
    f.write(''.join(level))
    f.close()

if __name__ == '__main__':
    numGhosts = np.random.randint(maxGhosts+1,size=1)[0]
    numCapsules = np.random.randint(maxCapsules+1, size=1)[0]
    folder = 'layouts'
    levelNumber = 0
    if sys.platform.startswith('win'):
        sep = '\\'
    else:
        sep = '/'
    folder += sep
    level = CreateLevel(numGhosts, numCapsules, gridSize, levelMap)
    WriteLevel(level, folder, lvl=levelNumber)