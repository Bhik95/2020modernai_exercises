# Add this to the search Agents for it to work (you also need to import the libraries defined here.
import numpy as np
import copy
class RHEAgent(Agent):
    "An agent that does RHE."

    def __init__(self):
        np.random.seed(100) #change seed value to true in pacman
        self.length = 15
        self.actions = [Directions.WEST, Directions.EAST, Directions.SOUTH, Directions.NORTH]
        self.generations = 180
        self.plan = self.RandomPlan()
        self.mutationRate = 0.9
        self.discount = 1
        self.callCount = 0



    def getAction(self, state):
        "The agent receives a GameState (defined in pacman.py)."
        self.callCount += 1
        fitness = self.getFitness(self.plan, state)
        legalMoves = state.getLegalPacmanActions()
        for i in range(self.generations):
            "*** YOUR CODE HERE ***"
            # mutate the offspring from the current plan.
            # then check its fitness, if this fitness is greater than the current fitness,
            # then the new plan is the current offspring


        action = self.plan.pop(0)
        self.plan.append(self.RandomAction())
        # append a random action after the pop so that the plan is always the length it should be.
        # You should check that the action you are using is in a legal move.
        # If it is not you can pop the plan until you have one action that is legal.

        return action

        # Filter impossible moves



    def getFitness(self,plan,state):
        nextState = state
        fitness = state.getScore()
        prevScore = fitness
        for actionIndex in range(len(plan)):
            action = plan[actionIndex]
            "*** YOUR CODE HERE ***"
            # simulate the environment by using the generateSuccessor function. check if the action makes you lose or win.
            # (state.isWin() let you check this. you can here simulate the ghosts and see if pacman dies.
            # (using the simulateGhost function defined in this class)


        return nextState.getScore()


    def getdiscountedChange(self, state, prevScore, index):
        "*** YOUR CODE HERE ***"
        # Generate a discounted value from state.getScore()

        newScore = state.getScore()
        return  newScore

    def simulateGhost(self,position, pacmanPosition, index, state):
        # This was created based on how the directional ghosts move in the code.
        legalActions = state.getLegalActions(index)

        bestDistance = np.inf
        bestAction = None

        for action in legalActions:
            actionVector = Actions.directionToVector(action, 1)
            newPosition = (position[0] + actionVector[0], position[1] + actionVector[1])

            distance = util.manhattanDistance(newPosition, pacmanPosition)
            if distance < bestDistance:
                bestDistance = distance
                bestAction = action

        return state.generateSuccessor(index, bestAction)


    def Mutate(self, plan, state):
        offspring = copy.copy(plan)
        nextState = state
        for i in range(len(plan)):
            if(nextState.isWin() or nextState.isLose()):
                return offspring
            action = offspring[i]
            legalMoves = nextState.getLegalPacmanActions()
            legalMoves.remove('Stop')
            "*** YOUR CODE HERE ***"
            # Check if the action is in the legal moves, if it is not change the action to a legal move,
            # if it is in the legal moves, then check if it mutates you can use np.random.random_sample()
            # to simulate a random number 0-1. generate then the next state (you have to use generateSuccessor
        return offspring

    def RandomPlan(self):
        return list(np.random.choice(self.actions,self.length,True))
    def RandomAction(self):
        return np.random.choice(self.actions)
    def RandomLegalAction(self, legalActions):
        return np.random.choice(legalActions)