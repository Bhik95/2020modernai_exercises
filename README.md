# 2020ModernAI_Exercises

This is a project for ITU's 2020 Modern AI course's exercises. 

The exercises will be focusing on Pacman's python 3 version by jspacco. Some exercise during the course might not use this setup so I will only upload the search folder for the project, but you should check it out if you are curious. There are many interesting exercises to do there. 

If you have any questions about this proyect you can send me a mail to luisferlaris@gmail.

Make sure if you fork this repo to **NOT** make your solutions public since this project is used by many profesors to teach and it contains good exercises that students can use to learn. 

Have fun looking into the exercises and hope you are able to have as much fun as I had making these exercises!

Thanks also to Toke Faurby https://github.com/Faur for also giving inspirational code to create these exercises!

## Exercise 1
For exercise 1 we are going to be creating a behaviour tree for Pacman. To do this there is a class in ./search/BehaviouTree.py this class has an idea on how to do a selector and a sequential node. it also has some basic nodes in check capsules and bad search. It is important to note that these do not work as they are because they use implemented searches from the original code, so it is encouraging for you to find a way to create you own check capsules to make Pacman go to the closest capsules.

Still there is an example of the tree we are looking to create in ./search/SearchAgents.py In here you will find the BehaviourTreeAgent which has an example of a tree with a selector as its main node and that goes to four different branches, each to check if Pacman should go to any direction. To make this work you need to create the classes Valid, Danger, GO in ./search/BehaviourTree.py 

The idea is that in the end you will have the following tree (which will make a very dumb Pacman, but is a first step!)
![](https://i.imgur.com/gA8GNlI.png)